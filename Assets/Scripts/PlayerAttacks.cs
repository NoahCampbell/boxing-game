using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttacks : MonoBehaviour
{
    public PlayerMaster playerMaster;
    public PlayerMovement playerMovement;
    public GameObject enemy;
    public EnemyHealth enemyHealth;
    public EnemyMovement enemyMovement;
    public bool isAttacking;

    // Start is called before the first frame update
    void Start()
    {
        playerMaster = gameObject.GetComponent<PlayerMaster>();
        playerMovement = gameObject.GetComponent<PlayerMovement>();
        enemy = GameObject.FindWithTag("Enemy");
        enemyMovement = enemy.GetComponent<EnemyMovement>();
        enemyHealth = enemy.GetComponent<EnemyHealth>();

        isAttacking = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Attack(bool strongAttack)
    {
        if (strongAttack)
        {
            StartCoroutine(StrongAttackAnimation());
        }
        else
        {
            StartCoroutine(WeakAttackAnimation());
        }
    }

    IEnumerator WeakAttackAnimation()
    {   
        isAttacking = true;
        playerMovement.canMove = false;

        var originalPosition = transform.position;
        transform.position += transform.forward * playerMaster.attackRange;

        if (IsTouchingEnemy())
        {
            var direction = enemy.transform.position - transform.position;
            direction.Normalize();
            enemyHealth.TakeDamage(playerMaster.weakAttackDamage);
            enemyMovement.Recoil("weak", direction);
        }

        yield return new WaitForSeconds(playerMaster.weakAttackSpeed);
        transform.position = originalPosition;

        isAttacking = false;
        playerMovement.canMove = true;
    }

    IEnumerator StrongAttackAnimation()
    {
        isAttacking = true;
        playerMovement.canMove = false;

        var originalPosition = transform.position;
        transform.position = transform.forward * playerMaster.attackRange;

        if (IsTouchingEnemy())
        {
            var direction = enemy.transform.position - transform.position;
            direction.Normalize();
            enemyHealth.TakeDamage(playerMaster.strongAttackDamage);
            enemyMovement.Recoil("strong", direction);
        }

        yield return new WaitForSeconds(playerMaster.strongAttackSpeed);
        transform.position = originalPosition;

        isAttacking = false;
        playerMovement.canMove = true;
    }

    public bool IsTouchingEnemy()
    {
        if (Vector3.Distance(transform.position, enemy.transform.position) <= 0.6)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
