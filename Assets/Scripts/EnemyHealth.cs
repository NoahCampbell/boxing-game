using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public EnemyMaster enemyMaster;

    // Start is called before the first frame update
    void Start()
    {
        enemyMaster = GetComponent<EnemyMaster>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(float damage)
    {
        enemyMaster.health -= damage;
        if (enemyMaster.health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        Destroy(gameObject);
    }

}
