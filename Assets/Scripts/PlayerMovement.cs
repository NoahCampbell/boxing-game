using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public PlayerMaster playerMaster;
    public PlayerAttacks playerAttacks;
    public bool canMove;
    public float timeHoldingAttack;

    // Start is called before the first frame update
    void Start()
    {
        playerMaster = GetComponent<PlayerMaster>();
        playerAttacks = GetComponent<PlayerAttacks>();

        canMove = true;
        timeHoldingAttack = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            CheckMovement();
        }
    }

    public void CheckMovement()
    {
        // Moves player according to WASD keys
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * playerMaster.speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.forward * -playerMaster.speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.right * -playerMaster.speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * playerMaster.speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift))
        {
            if (playerMaster.sprintTimer > 0)
            {
                playerMaster.speed = playerMaster.sprintSpeed;
                playerMaster.sprintTimer -= Time.deltaTime;
            }
            else
            {
                playerMaster.speed = playerMaster.walkSpeed;
            }
        }
        else
        {
            playerMaster.speed = playerMaster.walkSpeed;

            if (playerMaster.sprintTimer <= playerMaster.sprintCooldown)
            {
                playerMaster.sprintTimer += Time.deltaTime;
            }
        }

        // Rotates player according to Q and E keys
        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(-transform.up, playerMaster.rotateSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(transform.up, playerMaster.rotateSpeed * Time.deltaTime);
        }

        // Attacks player according to mouse click
        if (Input.GetKey(KeyCode.Space))
        {
            timeHoldingAttack += Time.deltaTime;
        }
        else
        {
            if (timeHoldingAttack > playerMaster.minStrongTime)
            {
                playerAttacks.Attack(true);
                timeHoldingAttack = 0f;
            }
            else if (timeHoldingAttack > 0)
            {
                playerAttacks.Attack(false);
                timeHoldingAttack = 0f;
            }
        }
    }

    public void Recoil(string attackType, Vector3 direction)
    {
        StartCoroutine(RecoilAnimation(attackType, direction));
    }

    IEnumerator RecoilAnimation(string attackType, Vector3 direction)
    {
        var timer = 0f;
        if (attackType == "strong")
        {
            timer = 200f;
        }
        else
        {
            timer = 100f;
        }

        canMove = false;
        transform.Translate(direction);

        while (timer > 0)
        {
            timer -= 0.2f;
            yield return null;
        }

        canMove = true;
    }

}
