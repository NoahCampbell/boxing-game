using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public PlayerMaster playerMaster;

    // Start is called before the first frame update
    void Start()
    {
        playerMaster = GetComponent<PlayerMaster>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(float damage)
    {
        playerMaster.health -= damage;
        if (playerMaster.health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        Destroy(gameObject);
    }
}
