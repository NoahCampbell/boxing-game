using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttacks : MonoBehaviour
{
    public EnemyMaster enemyMaster;
    public EnemyMovement enemyMovement;
    public GameObject player;
    public PlayerHealth playerHealth;
    public PlayerMovement playerMovement;
    public bool isWeakAttack;
    public bool isAttacking;

    // Start is called before the first frame update
    void Start()
    {
        enemyMaster = GetComponent<EnemyMaster>();
        enemyMovement = GetComponent<EnemyMovement>();
        player = GameObject.FindWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        playerMovement = player.GetComponent<PlayerMovement>();

        isWeakAttack = true;
        isAttacking = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Attack()
    {
        if (!isAttacking)
        {
            if (isWeakAttack)
            {
                StartCoroutine(WeakAttackAnimation());
            }
            else
            {
                StartCoroutine(StrongAttackAnimation());
            }
        }
    }

    IEnumerator WeakAttackAnimation()
    {   
        isAttacking = true;
        enemyMovement.canMove = false;

        var originalPosition = transform.position;
        transform.position += transform.forward * enemyMaster.attackRange;
        
        if (IsTouchingPlayer())
        {
            var direction = player.transform.position - transform.position;
            direction.Normalize();
            playerMovement.Recoil("weak", direction);
            playerHealth.TakeDamage(enemyMaster.weakAttackDamage);
        }

        yield return new WaitForSeconds(enemyMaster.weakAttackSpeed);
        transform.position = originalPosition;

        isAttacking = false;
        enemyMovement.canMove = true;
    }

    IEnumerator StrongAttackAnimation()
    {
        isAttacking = true;
        enemyMovement.canMove = false;

        var originalPosition = transform.position;
        transform.position += transform.forward * enemyMaster.attackRange;

        if (IsTouchingPlayer())
        {
            var direction = transform.position - player.transform.position;
            direction.Normalize();
            playerMovement.Recoil("strong", direction);
            playerHealth.TakeDamage(enemyMaster.strongAttackDamage);
        }

        yield return new WaitForSeconds(enemyMaster.strongAttackSpeed);
        transform.position = originalPosition;

        isAttacking = false;
        enemyMovement.canMove = true;
    }

    public bool IsTouchingPlayer()
    {
        if (Vector3.Distance(transform.position, player.transform.position) <= 1.2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
