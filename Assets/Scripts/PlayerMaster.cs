using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMaster : MonoBehaviour
{
    [Header("Player")]
    [System.NonSerialized] public float speed = 2f;
    [System.NonSerialized] public float sprintSpeed = 4f;
    [System.NonSerialized] public float walkSpeed = 2f;
    [System.NonSerialized] public float sprintTimer = 3f;
    [System.NonSerialized] public float sprintCooldown = 3f;
    [System.NonSerialized] public float rotateSpeed = 50f;
    [System.NonSerialized] public float health = 100f;
    [System.NonSerialized] public float maxHealth = 100f;
    [System.NonSerialized] public float weakAttackDamage = 2f;
    [System.NonSerialized] public float strongAttackDamage = 6f;
    [System.NonSerialized] public float defense = 0f;
    [System.NonSerialized] public float weakAttackSpeed = 0.2f;
    [System.NonSerialized] public float strongAttackSpeed = 3f;
    [System.NonSerialized] public float attackRange = 0.5f;
    [System.NonSerialized] public float minStrongTime = 2f;
}
