using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMaster : MonoBehaviour
{
    [Header("Enemy")]
    [System.NonSerialized] public float speed = 1f;
    [System.NonSerialized] public float sprintSpeed = 3f;
    [System.NonSerialized] public float sprintTimer = 4f;
    [System.NonSerialized] public float rotationSpeed = 50f;
    [System.NonSerialized] public float health = 100f;
    [System.NonSerialized] public float maxHealth = 100f;
    [System.NonSerialized] public float weakAttackDamage = 2f;
    [System.NonSerialized] public float strongAttackDamage = 6f;
    [System.NonSerialized] public float attackDistance = 2f;
    [System.NonSerialized] public float defense = 0f;
    [System.NonSerialized] public float weakAttackSpeed = 0.2f;
    [System.NonSerialized] public float strongAttackSpeed = 3f;
    [System.NonSerialized] public float attackChance = 1f;
    [System.NonSerialized] public float attackRange = 0.5f;
    [System.NonSerialized] public float sightRange = 15f;
    [System.NonSerialized] public float minWalkDistance = 3f;
    [System.NonSerialized] public float maxDashDistance = 7f;
    [System.NonSerialized] public float minDashDistance = 3f;
    [System.NonSerialized] public float minDistance = 1f;
}
