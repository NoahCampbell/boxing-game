using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public EnemyMaster enemyMaster;
    public EnemyAttacks enemyAttacks;
    public bool canMove;

    // Start is called before the first frame update
    void Start()
    {
        enemyMaster = GetComponent<EnemyMaster>();
        enemyAttacks = GetComponent<EnemyAttacks>();
        canMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            Movement();
        }
    }

    public Vector3 CheckForPlayer()
    {
        var isHit = false;

        for (int i = 0; i < 100; i++)
        {
            RaycastHit hit;
            Vector3 rayDirection = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
            Debug.DrawRay(transform.position, rayDirection * enemyMaster.sightRange, Color.red, 0f);
            isHit = Physics.Raycast(transform.position, rayDirection, out hit, enemyMaster.sightRange);

            if (isHit && hit.collider.gameObject.tag == "Player")
            {
                return hit.collider.gameObject.transform.position;
            }
        }

        return Vector3.zero;
    }

    public void Movement()
    {
        var targetPosition = CheckForPlayer();

        if (targetPosition != Vector3.zero)
        {
            // Debug.Log(Vector3.Distance(transform.position, targetPosition));

            transform.LookAt(targetPosition);

            var distance = Vector3.Distance(transform.position, targetPosition);

            if (distance >= enemyMaster.maxDashDistance)
            {
                Dash(targetPosition);
            }
            else if (distance > enemyMaster.minWalkDistance)
            {
                MoveTo(targetPosition);
            }

            if (distance <= enemyMaster.minDistance)
            {
                MoveBackwards();
            }

            if (distance <= enemyMaster.attackDistance && Random.Range(0, 1000) < enemyMaster.attackChance)
            {
                enemyAttacks.Attack();
            }

            if (distance < enemyMaster.minWalkDistance && distance > enemyMaster.minDistance)
            {
                MoveSideways();
            }
        }
        else 
        {
           MoveForward();
        }
    }

    public void MoveTo(Vector3 targetPosition)
    {
        var direction = transform.position - targetPosition;
        direction.Normalize();
        transform.Translate(direction * enemyMaster.speed * Time.deltaTime);
    }

    public void MoveForward()
    {
        transform.Translate(Vector3.forward * enemyMaster.speed * Time.deltaTime);
    }

    public void MoveBackwards()
    {
        transform.Translate(Vector3.forward * -enemyMaster.speed * Time.deltaTime);
    }

    public void MoveSideways()
    {
        transform.Translate(Vector3.right * enemyMaster.speed * Time.deltaTime);
    }

    public void Recoil(string attackType, Vector3 direction)
    {
        StartCoroutine(RecoilAnimation(attackType, direction));
    }

    public void Dash(Vector3 targetPosition)
    {
        var direction = transform.position - targetPosition;
        direction.Normalize();
        transform.Translate(direction * enemyMaster.sprintSpeed * Time.deltaTime);
    }

    IEnumerator RecoilAnimation(string attackType, Vector3 direction)
    {
        var timer = 0f;
        if (attackType == "strong")
        {
            timer = 100f;
        }
        else
        {
            timer = 50f;
        }

        canMove = false;
        transform.Translate(direction);

        while (timer > 0)
        {
            timer -= 0.2f;
            yield return null;
        }

        canMove = true;
    }

}
